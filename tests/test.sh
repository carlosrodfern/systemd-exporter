#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "dnf install -y ./data/systemd-exporter.rpm"
        rlServiceStart "systemd-exporter.service"
    rlPhaseEnd

    rlPhaseStartTest
        rlWaitForCmd "curl -s http://localhost:9558/" -t 60
        rlRun -s "curl -s -w '%{http_code}' http://localhost:9558/"
        rlAssertGrep "200" $rlRun_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceStop "systemd-exporter.service"
        rlRun "dnf remove -y systemd-exporter"
    rlPhaseEnd
rlJournalEnd
