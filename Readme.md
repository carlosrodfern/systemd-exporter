# How to build locally

```sh
spectool -g systemd-exporter.spec
rpkg srpm --outdir .
mock -r centos-stream-9-x86_64 --init
mock --enable-network -r centos-stream-9-x86_64 *.src.rpm
```

# How to test

```sh
cp /var/lib/mock/centos-stream-9-x86_64/result/systemd-exporter-0.6.0-1.el9.x86_64.rpm ./tests/data/systemd-exporter.rpm
tmt -vvv run
```