# https://github.com/prometheus-community/systemd_exporter
%global goipath         github.com/prometheus-community/systemd_exporter
Version:                0.6.0
%global tag             v0.6.0

%gometa -f

%global goname systemd-exporter

%global common_description %{expand:
Prometheus exporter for systemd units.}

%global golicenses      LICENSE
%global godocs          CHANGELOG.md CODE_OF_CONDUCT.md\\\
                        README.md SECURITY.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Prometheus exporter for systemd units

License:        Apache-2.0
URL:            %{gourl}
Source0:        %{gosource}
Source1:        %{goname}.service
Source2:        %{goname}.conf

BuildRequires:  systemd-rpm-macros

%description %{common_description}

%gopkg

%prep
%goprep -k
%autopatch -p1

%build
export GO111MODULE=on
%gobuild -o %{gobuilddir}/bin/%{goname} %{goipath}

%install
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
install -m 0644 -vpD %{S:1} %{buildroot}%{_unitdir}/%{goname}.service
install -m 0644 -vpD %{S:2} %{buildroot}%{_sysconfdir}/default/%{goname}
mkdir -vp %{buildroot}/%{_mandir}/man1/
%{buildroot}%{_bindir}/%{goname} --help-man > %{buildroot}/%{_mandir}/man1/%{goname}.1
sed -i '/^  /d; /^.SH "NAME"/,+1c.SH "NAME"\nsystemd-exporter \\- The Systemd Exporter for Prometheus' \
    %{buildroot}/%{_mandir}/man1/%{goname}.1

%pre
%sysusers_create_compat %{SOURCE1}

%post
%systemd_post %{goname}.service

%preun
%systemd_preun %{goname}.service

%postun
%systemd_postun_with_restart %{goname}.service

%files
%license LICENSE
%doc CHANGELOG.md CODE_OF_CONDUCT.md README.md
%doc SECURITY.md
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/default/%{goname}
%{_unitdir}/%{goname}.service
%{_mandir}/man1/%{goname}.1*

%changelog
%autochangelog
